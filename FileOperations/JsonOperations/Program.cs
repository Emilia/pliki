﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JsonOperations
{
    class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public List<Phone> Phones { get; set; }
    }

    class Phone
    {
        public string Type { get; set; }
        public string Number { get; set; }
    }

    class Program
    {
        const string jsonFile = "Data/JsonExample.json";

        //deserialize JavaScriptSerialize

        static void DeserializeUsingJavaScriptSerializer()
        {
            string json = File.ReadAllText(jsonFile);
            var jsonObject = new JavaScriptSerializer().Deserialize<Person>(json);
            Console.WriteLine(jsonObject.Name);
            foreach (var phone in jsonObject.Phones)
            {
                Console.WriteLine(phone.Number);
            }
        }

        //serialize with JavaScriptSerialize
        static void SerializeUsingJavaScriptSerializer()
        {
            Person person = new Person();
            person.Name = "Ola";
            person.Surname = "Kowalska";
            person.Age = 30;
            
            var json = new JavaScriptSerializer().Serialize(person);
            Console.WriteLine(json);
        }

        //using JSON.NEt //Install-Package Newtonsoft.Json -Version 11.0.1

        static void DeserializeUsingJsonNet()
        {
            string json = File.ReadAllText(jsonFile);
            var jsonObj = JsonConvert.DeserializeObject<Person>(json);
            Console.Write(jsonObj.Name);
        }

        static void SerializeUsingJsonNet()
        {
            Person person = new Person();
            person.Name = "Ola";
            person.Surname = "Kowalska";
            person.Age = 30;

            var json = JsonConvert.SerializeObject(person);
            Console.WriteLine(json);
        }

        //Parsing JSON
        static void ParseJson()
        {
            string json = File.ReadAllText(jsonFile);
            JObject jsonObject = JObject.Parse(json);
            Console.WriteLine(jsonObject["Name"]);
            Console.WriteLine(jsonObject["Phones"][0]["Type"]);
        }


        static void Main(string[] args)
        {
            //DeserializeUsingJavaScriptSerializer();
            SerializeUsingJavaScriptSerializer();
            //DeserializeUsingJsonNet();
            //SerializeUsingJsonNet();
            //ParseJson();
            Console.ReadKey();
        }
    }
}

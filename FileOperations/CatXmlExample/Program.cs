﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace CatXmlExample
{
    public class Program
    {
        public class Kot
        {
            public string Imie { get; set; }
            public int Wiek { get; set; }
            public string Wlasciciel { get; set; }
        }

        static void CatSerializeDeserialize()
        {
            Kot kot = new Kot();

            XElement xml = XElement.Load("Kot.xml");
            kot.Imie = xml.Element("Imie").Value;
            kot.Wiek = Int32.Parse(xml.Element("Wiek").Value);
            kot.Wlasciciel = xml.Element("Wlasciciel").Value;

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Kot), new XmlRootAttribute("Kot"));

            StringBuilder stringBuilder = new StringBuilder();
            xmlSerializer.Serialize(new StringWriter(stringBuilder), kot);

            Console.WriteLine(stringBuilder);
        }

        static void Main(string[] args)
        {
            CatSerializeDeserialize();
            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatExample
{
    class Program
    {
        static void ReadLineByLine()
        {
            Dictionary<string, List<string>> cats = new Dictionary<string, List<string>>();
            int suma = 0;
            int lineNumber = 0;
            string[] headers = null;
            foreach (var line in File.ReadLines("cats.csv"))
            {
                if (lineNumber == 0)
                {
                    headers = line.Split(',');
                    foreach (var header in headers)
                    {
                        cats.Add(header, new List<string>());
                    }
                }
                else
                {
                    string[] values = line.Split(',');
                    for (int i = 0; i < values.Length; i++)
                    {
                        cats[headers[i]].Add(values[i]);
                    }
                    
                }
                lineNumber++;
            }

            foreach (var wiek in cats["wiek"])
            {
                suma += Int32.Parse(wiek);
            }

            Console.WriteLine(suma/3);
        }
        static void Main(string[] args)
        {
            ReadLineByLine();
            Console.ReadKey();
        }
    }
}

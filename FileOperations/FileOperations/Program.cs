﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileOperations
{
    class Program
    {
        private const string fileName = "Data/ExampleFile.txt";

        //czytanie całego pliku
        static void ReadAllFile()
        {
            var fileContent = File.ReadAllText(fileName);
            Console.WriteLine(fileContent);
        }
        
        //czytanie całego pliku za pomocą streamów
        static void ReadAllFileStream()
        {
            string fileContent;
            FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                fileContent = streamReader.ReadToEnd();
            }

            Console.WriteLine(fileContent);
        }

        //czytanie całego pliku do tablicy

        static void ReadAllFileToArray()
        {
            string[] fileContentArray = File.ReadAllLines(fileName);
            foreach (var fileContent in fileContentArray)
            {
                Console.WriteLine(fileContent);
            }
        }
        //czytanie po kolei każdą z linijek

        static void ReadLineByLine()
        {
            foreach (var line in File.ReadLines(fileName))
            {
                Console.WriteLine(line);
            }
        }
        //czytanie po kolei każdą z linijek za pomocą streamów

        static void ReadLineByLineUsingStreams()
        {
            FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
            }
        }

        //zapisywanie do pliku

        static void WriteToFile()
        {
            File.WriteAllText(fileName, "udalo sie zapisac!");
            Console.WriteLine(File.ReadAllText(fileName));
        }

        //dodawanie treści do istniejącego pliku

        static void AppendToFile()
        {
            File.AppendAllText(fileName, "append text");
            Console.WriteLine((File.ReadAllText(fileName)));
        }

        static void Main(string[] args)
        {
            ReadAllFile();
            ReadAllFileStream();
            ReadAllFileToArray();
            ReadLineByLine();
            ReadLineByLineUsingStreams();
            WriteToFile();
            AppendToFile();

            Console.ReadKey();
        }
    }
}

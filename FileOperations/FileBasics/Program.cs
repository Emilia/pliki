﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileBasics
{
    class Program
    {
        //Tworzenie folderu i pliku
        static void CreateDirectoryAndFile(string pathToDir, string pathToFile)
        {
            var directory = Directory.CreateDirectory(pathToDir);

            if (directory.Exists)
            {
                using (File.Create(pathToFile))
                {
                }
                
                if (File.Exists(pathToFile))
                {
                    Console.WriteLine("File exists");
                }
            }
        }

        //Informacje o pliku/folderze

        static void GetDirectoryAndFileInfo(string pathToDir, string pathToFile)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(pathToDir);
            Console.WriteLine(directoryInfo.Name);
            FileInfo fileInfo = new FileInfo(pathToFile);
            Console.WriteLine(fileInfo.Name);
        }

        //Wylistowanie plikow z okreslonej sciezki & wylistowanie tylko plikow o danym rozszerzeniu

        static void ListAllFilesFromDirectory(string pathToDir)
        {
            string[] files = Directory.GetFiles(pathToDir);
            foreach (var file in files)
            {
                Console.WriteLine(file);
            }

            files = Directory.GetFiles(pathToDir, "*.bmp");
            foreach (var file in files)
            {
                Console.WriteLine(file);
            }
        }

        //Kasowanie pliku

        static void RemoveFile(string pathToFile)
        {
            File.Delete(pathToFile); 
            
            if (!File.Exists(pathToFile))
            {
                Console.WriteLine("File removed");
            }
        }


        static void Main(string[] args)
        {
            var currentDirectory = Environment.CurrentDirectory;
            var pathToDir = Path.Combine(currentDirectory, "Directory");
            var pathToFile = Path.Combine(pathToDir, "file.txt");

            CreateDirectoryAndFile(pathToDir, pathToFile);
            GetDirectoryAndFileInfo(pathToDir, pathToFile);
            ListAllFilesFromDirectory(pathToDir);
            RemoveFile(pathToFile);

            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace XmlOperations
{
    public class Program
    {
        //[XmlRoot("Person")]
        public class Person
        {
            //[XmlElement("Imie")]
            public string Name { get; set; }
            public string Surname { get; set; }
            public int Age { get; set; }
            public Address Address { get; set; }
            public List<Number> Phones { get; set; }
        }

        public class Number
        {
            [XmlAttribute]
            public string Type { get; set; }
            [XmlText]
            public string Value { get; set; }
        }

        public class Address
        {
            public string Street { get; set; }
            public string City { get; set; }
        }

        public const string xmlFile = "Data/XmlExample.xml";

        //XmlReader (XmlTextReader - obsolete)
        static void ParseUsingXmlReader()
        {
            XmlReader xmlReader = XmlReader.Create(new StreamReader(xmlFile));
            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element:
                    {
                        Console.WriteLine("Element: " + xmlReader.Name);
                        if (xmlReader.HasAttributes)
                        {
                            var attribute = xmlReader.GetAttribute("Type");
                            Console.WriteLine("Atrybut: " + attribute);
                        }
                        break;
                    }
                    case XmlNodeType.Text:
                    {
                        Console.WriteLine("Text: " + xmlReader.Value);
                        break;
                    }
                }
            }
        }

        static void ParsingUsingXmlDocument()
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(xmlFile);
            var element = xmlDocument.GetElementsByTagName("Street").Item(0).InnerText;
            var elements = xmlDocument.GetElementsByTagName("Street");
            foreach (XmlNode elementO in elements)
            {
                Console.WriteLine(elementO.Name);
            }
            Console.WriteLine(element);

            XmlNodeList nodeList = xmlDocument.DocumentElement.SelectNodes("/Person/Address/Street");
            foreach (XmlNode node in nodeList)
            {
                Console.WriteLine(node.InnerText);
            }
        }

        static void ParsingUsingXmlLinq()
        {
            var xmlDocument = XDocument.Load(xmlFile);
            var attribute = xmlDocument.Descendants("Address").Select(x => x.Element("Street")).FirstOrDefault();
            Console.WriteLine(attribute);
            
            var xmlElement = XElement.Load(xmlFile).Element("Name").Value;
            Console.WriteLine(xmlElement);
        }

        //deserialize xml
        static void DeserializeXml()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Person), new XmlRootAttribute("Person"));
            Person xmlObj = (Person)xmlSerializer.Deserialize(new StreamReader(xmlFile));
            Console.WriteLine(xmlObj.Name);
        }

        static void SerializeXml()
        {
            Person person = new Person();
            person.Name = "Ola";
            person.Surname = "Kowalska";
            person.Age = 30;

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Person), new XmlRootAttribute("Person"));
            StringBuilder stringBuilder = new StringBuilder();
            xmlSerializer.Serialize(new StringWriter(stringBuilder), person );
            Console.WriteLine(stringBuilder);
        }

        static void Main(string[] args)
        {
            //ParsingUsingXmlDocument();
            //ParseUsingXmlReader();
          //ParsingUsingXmlLinq();
            //DeserializeXml();
            SerializeXml();
            Console.ReadKey();
        }
    }
}
